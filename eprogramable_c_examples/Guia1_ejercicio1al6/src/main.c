/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Guia1_ejercicio1al6/inc/main.h"

#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

int main(void)
{
	int32_t a=0;
	a=1<<5;					//Ejercicio 1 Declare una constante de 32 bits, con todos los bits en 0
	printf("%d \r\n",a);	//y el bit 6 en 1. Utilice el operador <<.


	//Ejercicio 2Declare una constante de 16 bits, con todos los bits en 0 y los bits 3 y 4 en 1.
	//Utilice el operador <<

	a=(int16_t) a;
	a=3<<2;
	printf("%d \r\n",a);

	//Ejercicio 3Declare una variable de 16 bits sin signo, con el valor inicial 0xFFFF y luego, mediante una
	//operación de máscaras, coloque a 0 el bit 14.

	a=(uint16_t)a;
	a=0xFFFF;
	uint16_t b=0;
	b=1<<13;
	b=~b;
	a=a&b;
	printf("%d \r\n",a);

	//ejercicio 4 Declare una variable de 32 bits sin signo, con el valor inicial 0x0000 y luego, mediante una
	//operación de máscaras, coloque a 1 el bit 2

	a=(uint32_t)a;
	a=0x0000;
	b=(uint32_t) b;
	b=1<<1;
	a=a|b;
	printf("%d \r\n",a);


	//ejercicio 5 Declare una variable de 32 bits sin signo, con el valor inicial 0x00001234 y luego, mediante
	//una operación de máscaras, invierta el bit 0.

	a=0x00001234;
	b=1;
	a=a^b;
	printf("%d \r\n",a);

	//Sobre una variable de 32 bits sin signo previamente declarada y de valor desconocido,
	//asegúrese de colocar el bit 4 a 0 mediante máscaras y el operador <<.
	a=0xFFFFFFFF;
	b=1<<3;
	b=~b;
	a=a&b;
	printf("%d \r\n",a);






return 0;}

/*==================[end of file]============================================*/

