/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>


/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

typedef struct {
						uint8_t byte4;
						uint8_t byte3;
						uint8_t byte2;
						uint8_t byte1;
										}cada_byte;

	union data{
			cada_byte bytes;
			uint32_t todos_los_bytes;
	}valores;


int main(void)
{
   /*EJERCICIO 16:
    a. Declare una variable sin signo de 32 bits y cargue el valor 0x01020304. Declare
		cuatro variables sin signo de 8 bits y, utilizando máscaras, rotaciones y truncamiento,
		cargue cada uno de los bytes de la variable de 32 bits.
	b. Realice el mismo ejercicio, utilizando la definición de una “union”*/

	uint32_t a;
	a=0x01020304;

	uint8_t b1=0,b2=0,b3=0,b4=0;

	uint16_t aux1=0;

	aux1=(uint16_t) a; //tomo los 16 menos significativos de a
	b4=(uint8_t) aux1; //tomo los 8 menos significativos de aux
	aux1=aux1>>8;	   //desplazo y quedan los 8 mas significativos
	b3=(uint8_t) aux1; //copio los 8 mas significativos de la primer mitad de a

 //modifico el valor de a, aunque se podria no modificarlo usando una variable auxiliar
	a=a>>16;
	aux1=(uint16_t) a;	   //tomo los 16 bit menos significativos de a
	b2=(uint8_t) aux1;
	aux1=aux1>>8;
	b1=(uint8_t) aux1;

	printf("%d \r\n%d \r\n%d \r\n%d \r\n%d \r\n",a,b1,b2,b3,b4);



	valores.todos_los_bytes=0x01020304;
//al ser una union, modificando un solo valor, se modifican los otros 4 variables que ocupan el mismo espacio
//	de memoria
	printf("%d \r\n%d \r\n%d \r\n%d \r\n%d \r\n",valores.todos_los_bytes,
			valores.bytes.byte1,valores.bytes.byte2,valores.bytes.byte3,valores.bytes.byte4);













	return 0;
}

/*==================[end of file]============================================*/

