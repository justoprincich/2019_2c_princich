/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
/*==================[macros and definitions]=================================*/
typedef struct
			{
				uint8_t n_led;		//indica el número de led a controlar
				uint8_t n_ciclos;   //indica la cantidad de cilcos de encendido/apagado
				uint8_t periodo; 	//indica el tiempo de cada ciclo
				uint8_t mode; 		//ON, OFF, TOGGLE considero ON=1, OFF=0
 			} my_leds;



/*==================[internal functions declaration]=========================*/

void manejo_leds(my_leds *leds){

		if(leds->mode==1){
			if(leds->n_led==1){printf("Enciendo led 1\n\r");}
			else{if(leds->n_led==2){printf("Enciendo led 2\n\r");}
				else{if(leds->n_led==3){printf("Enciendo led 3\n\r");}
					else{printf("No prendo ningun led\n\r");}}}
		}
		else{if(leds->mode==0){
				if(leds->n_led==1){printf("Apago led 1\n\r");}
						else{if(leds->n_led==2){printf("Apago led 2\n\r");}
							else{if(leds->n_led==3){printf("Apago led 3\n\r");}
								else{printf("No Apago ningun led\n\r");}}}}

				else{ if(leds->mode==2){
					int i=0;
					while(i<leds->n_ciclos){
						if(leds->n_led==1){printf("Toggleo led 1\n\r");}
												else{if(leds->n_led==2){printf("Toggleo led 2\n\r");}
													else{if(leds->n_led==3){printf("Toggleo led 3\n\r");}
														else{printf("No Toggleo ningun led\n\r");}}}
						i++;
					}
				}}
			}
		}

 //no se pone print dentro de una funcion estan para acordarme que era que hacia cada uno

int main(void)
{

my_leds led;
led.mode=2;
led.n_ciclos=5;
led.n_led=2;
led.periodo=2;

my_leds *pled;
pled=&led;


manejo_leds(pled);




	return 0;
}

/*==================[end of file]============================================*/

