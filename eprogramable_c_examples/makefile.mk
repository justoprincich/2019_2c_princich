########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe

#PROYECTO_ACTIVO = Guia1_ejercicio1al6
#NOMBRE_EJECUTABLE = Guia1_ejercicio1al6.exe

#PROYECTO_ACTIVO = Guia1_ejercicio7
#NOMBRE_EJECUTABLE = Guia1_ejercicio7.exe

#PROYECTO_ACTIVO = Guia1_ejercicio8
#NOMBRE_EJECUTABLE = Guia1_ejercicio8.exe

#PROYECTO_ACTIVO = Guia1_ejercicio9
#NOMBRE_EJECUTABLE = Guia1_ejercicio9.exe

#PROYECTO_ACTIVO = Guia1_ejercicio10
#NOMBRE_EJECUTABLE = Guia1_ejercicio10.exe

#PROYECTO_ACTIVO = Guia1_ejercicio11
#NOMBRE_EJECUTABLE = Guia1_ejercicio11.exe

PROYECTO_ACTIVO = Guia1_ejercicio12
NOMBRE_EJECUTABLE = Guia1_ejercicio12.exe
