/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

/*==================[macros and definitions]=================================*/
typedef struct
{
	uint8_t port; 			/*!< GPIO port number */
	uint8_t pin; 			/*!< GPIO pin number */
	uint8_t dir; 			/*!< GPIO direction ‘0’ IN; ‘1’ OUT */
	} gpioConf_t;


/*==================[internal functions declaration]=========================*/

void ControlPuerto(gpioConf_t *puertos, uint8_t bcd){
	int i=0;
	uint8_t aux=0;
	for(i=0;i<4;i++){

		aux=1<<i;
		aux=bcd&aux;
		if(aux!=0){
			printf("Puerto %d.%d Activo\n",puertos[i].port,puertos[i].pin);
		}
		else{printf("Puerto %d.%d Inactivo\n",puertos[i].port,puertos[i].pin);}
	}

}


int main(void)
{
	gpioConf_t puertos[4];
	puertos[0].port=1;
	puertos[1].port=1;
	puertos[2].port=1;
	puertos[3].port=2;
	puertos[0].pin=4;
	puertos[1].pin=5;
	puertos[2].pin=6;
	puertos[3].pin=14;
	puertos[0].dir=1;
	puertos[1].dir=1;
	puertos[2].dir=1;
	puertos[3].dir=1;


	ControlPuerto(puertos,5);


	return 0;
}

/*==================[end of file]============================================*/

