/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

/*
uint8_t BinBcd(uint8_t x){
	if(x==0){return 0000;}
	else{if(x==1){return 0001;}
		 else{if(x==2){return 0010;}
		 	  else{if(x==3){return 0011;}
		 	  	   else{if(x==4){return 0100;}
		 	  	   	    else{if(x==5){return 0101;}
		 	  	   	    	 else{if(x==6){return 0110;}
		 	  	   	    	 	  else{if(x==7){return 0111;}
		 	  	   	    	 	  	   else{if(x==8){return 1000;}
		 	  	   	    	 	  	   	    else{return 1001;}}}}}}}}}
}
*/


void binarioABcd(uint32_t data, uint8_t digits, uint8_t *bcd_number ){
	uint8_t i=0;
	while(i<digits){
		//el resto de la division entera por 10 te da el primer digito
		bcd_number[i]=data%10;
		data=data/10;
		i++;

	}

}



int main(void)
{
	uint32_t a=123;
	uint8_t dig=3;
	uint8_t bcd[dig];

	binarioABcd(a, dig, bcd);

	uint8_t i=0;

	while(i<dig){
		printf("digitos  %d\n", bcd[i]);
			i++;
	}



	return 0;
}

/*==================[end of file]============================================*/

