/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>


/*==================[macros and definitions]=================================*/
typedef struct
{
char *txt ; /* Etiqueta de la opción */
void (*doit)() ; /* Función a ejecutar en caso de seleccionar esa opción */
} menuEntry ;

/*==================[internal functions declaration]=========================*/
void Funcion1(){
	printf("ejecute funcion 1\n");
}

void Funcion2(){
	printf("ejecute funcion 2\n");
}

void Funcion3(){
	printf("ejecute funcion 3\n");
}




menuEntry menuPrincipal[3]={{"Opcion 1",Funcion1},{"Opcion 2",Funcion2},{"Opcion 3",Funcion3}};

void EjecutarMenu(menuEntry *menu, int opcion){
	menu[opcion-1].doit();
}


int main(void)
{

	EjecutarMenu(menuPrincipal,1);
	EjecutarMenu(menuPrincipal,2);
	EjecutarMenu(menuPrincipal,3);

	return 0;
}

/*==================[end of file]============================================*/

