/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <string.h>
#include <stdio.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

/* EJERCICIO 14:    Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres,
  “apellido” de 20 caracteres y edad.
a. Defina una variable con esa estructura y cargue los campos con sus propios datos.
b. Defina un puntero a esa estructura y cargue los campos con los datos de su
compañero (usando acceso por punteros).*/


typedef struct{
	char nombre[13];
	char apellido[21];
	char edad[3];
}ALUMNO;

int main(void)
{
	ALUMNO alum1; //ALUMNO alum1={"justo","princich","21"}; la declaras e inicializas

    strcpy( alum1.nombre, "justo");
    strcpy( alum1.apellido, "princich");
    strcpy( alum1.edad, "21");


    printf("Nombre: %s\r\nApellido: %s \r\nEdad: %s \r\n",alum1.nombre, alum1.apellido, alum1.edad);


    ALUMNO *alum2;
    alum2=&alum1;
    strcpy( alum2->nombre, "bruno");
    strcpy( alum2->apellido, "zorzet");
    strcpy( alum2->edad, "20");

    printf("\r\nNombre: %s\r\nApellido: %s \r\nEdad: %s \r\n",alum2->nombre, alum2->apellido, alum2->edad);






	return 0;
}

/*==================[end of file]============================================*/

