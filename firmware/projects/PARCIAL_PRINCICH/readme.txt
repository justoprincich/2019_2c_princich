﻿Parcial_ proyecto medicion de presion, comunicacion a computadora, mostrar por display y indicadores led
su funcionamiento esta comandado por las teclas de 2,3 y 4 de la EDU-CIAA

Se debe conectar el display lcd ITSE0803 en los siguientes pines de la placa: LCD1,LCD2,LCD3,LCD4,GPIO1,GPIO3,GPIO5
Los datos se enviar por uart a traves del puerto usb de la placa hacia la computadora a razon de 115200 baudios

Se debe conectar la señal de presion en el canal 1 del conversor analogico digital, se muestrea la señal a 250 Hz y se analiza de a tramos de 1 segundo

El boton  2 se usa para mostrar el maximo, enviarlo por la uart y prender led rojo indicativo que se esta mostrando el maximo
El boton  3 se usa para mostrar el minimo, enviarlo enviarlo por la uart y prender led amarillo indicativo que se esta mostrando el minimo
El boton  4 se usa para mostrar el promedio, enviarlo enviarlo por la uart y prender led verde indicativo que se esta mostrando el promedio

Si la presión máxima supera los 150 mmHg se enciende el red rojo del led RGB. 
Si la presion maxima esta entre 150 y 50 mmHg el led azul del RGB
Si la presion esta por debajo de 50 mmHg el led verde del RGB.

