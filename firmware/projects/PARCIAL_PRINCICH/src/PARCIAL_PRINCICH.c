/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/PARCIAL_PRINCICH.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "analog_io.h"
#include "timer.h"
#include "uart.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "bool.h"



/*==================[macros and definitions]=================================*/

#define SAMPLE_PERIOD_US 4000	 //4000 micro segundos representan una frecuencia de 250 Hz

#define WINDOW_WIDTH 250		// cantidad de muestras que tiene una ventana de 1 segundo

#define MAXIMO_ALERT 150		// maximo de comparacion para alarma luminica de presion

#define MINIMO_ALERT 50			// minimo de comparacion para alarma luminica de presion


/*==================[internal data definition]===============================*/

uint16_t senial[WINDOW_WIDTH];	 //vector que contendra el segundo se señal convertido

uint16_t aux_senial;			 //Variable auxiliar que contiene el dato a guardar en el vector señal

uint16_t contador_senial = 0;	 //Contador asociado a la posición del vector señal

bool new_window = false;		 //Booleano utilizado como bandera para indicar una nueva ventana(1 segundo de señal)

bool max_flag = false;           //Banderas que indican que se esta pidiendo mostrar
bool min_flag = false;
bool prom_flag = false;


/*==================[internal functions declaration]=========================*/

//INICIA LA CONVERSION DEL DATO
void ConvInt(void)
{
	//Función que comienza a convertir el dato AD, una vez que termina de convertir, llama a la función ReadAD()
	AnalogStartConvertion();
}

//FUNCION QUE SE LLAMA CUANDO SE TERMINA LA CONVERSION Y GUARDA EL DATO EN UN VECTOR
void ReadAD(void)
{
	//Lee dato y lo guarda en variable auxiliar
	AnalogInputRead(CH1, &aux_senial);

	//Asigna el valor de la variable auxiliar al componente del vector correspondiente
	if(contador_senial<WINDOW_WIDTH)
	{
		senial[contador_senial] = aux_senial*(200/1024); //guardo el valor de presion en el vector; ese producto hace el escalado

		contador_senial++; //Aumenta el valor del contador en uno
	}

		//Cuando el contador llega al máximo, se reinicia, y levanta bandera de que se tiene un segundo de señal
	else
	{
		contador_senial = 0;
		new_window = true;
	}
}

//FUNCION DE INTERRUPCION DE LA TECLA 2, MODIFICA BANDERAS PARA MOSTRAR EL MAXIMO
void Maximo(void){
	max_flag=true;
	min_flag=false;
	prom_flag=false;
}

//FUNCION DE INTERRUPCION DE LA TECLA 3, MODIFICA BANDERAS PARA MOSTRAR EL MINIMO
void Minimo(void){
	min_flag=true;
	max_flag=false;
	prom_flag=false;
}

//FUNCION DE INTERRUPCION DE LA TECLA 4, MODIFICA BANDERAS PARA MOSTRAR EL PROMEDIO
void Promedio(void){
	prom_flag=true;
	min_flag=false;
	max_flag=false;
}

//FUNCION DE QUE COMPARA EL VALOR DE PRESION CON LOS LIMITES Y PRENDE LEDS SEGUN CORRESPONDA
void Alarm(uint16_t max){
	if(max>MAXIMO_ALERT){  			//compara si el maximo es mayor que el de referencia
		LedOff(LED_RGB_B);
		LedOn(LED_RGB_R);
		LedOff(LED_RGB_G);
	}
	else{
		if(max<MINIMO_ALERT){		// compara si el maximo es menor que el de refencia
			LedOff(LED_RGB_B);
			LedOff(LED_RGB_R);
			LedOn(LED_RGB_G);
		}
		else{						//si no fue verdadera ninguna de las sentencias anteriores entonces esta entre medio de ellas
			LedOn(LED_RGB_B);
			LedOff(LED_RGB_R);
			LedOff(LED_RGB_G);
		}
	}
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();


	//CONFIGURAMOS EL CONVERSOR ANALOGICO DIGITAL
	analog_input_config ad_config;
	ad_config.input = CH1;					//Seteamos el canal 1
	ad_config.mode = AINPUTS_SINGLE_READ;
	ad_config.pAnalogInput = ReadAD;		//Funcion que se llama cuando se termina la conversion

	AnalogInputInit(&ad_config);


	//CONFIGURAMOS EL TEMPORIZADOR QUE DARA INICIO A LA CONVERSION
	timer_config temporizador;
	temporizador.period = SAMPLE_PERIOD_US; //Período de muestreo
	temporizador.timer = TIMER_A; 			//Se usa SysTick
	temporizador.pFunc = ConvInt;			//Se asigna la funcion que activara la conversion
	TimerInit(&temporizador);

	TimerStart(TIMER_A);					//Arranca el timer

	//CONFIGURAMOS LA PANTALLA
	gpio_t pines[]={LCD1,LCD2,LCD3,LCD4,GPIO1,GPIO3,GPIO5};	 // PINES PARA LA PANTALLA
	ITSE0803Init(pines);	 								 //inicializamos los pines para la pantalla

	//CONFIGURAMOS LOS BOTONES
	SwitchesInit();

	SwitchActivInt(SWITCH_2, Maximo);			//boton 2 esta abajo del led_1 rojo por tanto tiene que ser el maximo
	SwitchActivInt(SWITCH_3, Minimo);			//boton 3 esta abajo del led_2 amarillo por tanto tiene que ser el minimo
	SwitchActivInt(SWITCH_4, Promedio);			//boton 4 esta abajo del led_3 verde por tanto tiene que ser el promedio

	//INICIALIZAMOS LEDS
	LedsInit();

	//CONFIGURAMOS LA UART
	//enviamos los datos por el puerto usb de la placa, a 115200 baudios, no configuramos la recepcion de datos

	serial_config config_uart={SERIAL_PORT_PC,115200,NO_INT};

	UartInit(&config_uart.port);



    while(1)
    {
    	uint16_t i=0;
    	uint16_t promedio=0;
    	uint16_t maximo=0;
    	uint16_t minimo=0;

    	//cuando se completa un segundo de ventana new_window es true y se ejecuta el calculo
    	if(new_window){
    		maximo=senial[0];
    		minimo=senial[0];

    		for(i=0; i<WINDOW_WIDTH ; i++){				 //recorremos la señal haciendo la acumulacion sobre promedio y buscando maximo y minimo
    			promedio+=senial[i];

    			if(senial[i]<minimo){ minimo=senial[i];} //busco el minimo
    			if(senial[i]>maximo){ maximo=senial[i];} //busco el maximo
    		}
    		promedio=promedio/WINDOW_WIDTH;			//dividimos la sumatoria por la cantidad de valores

    		if(max_flag){      //muestra y envia el maximo y prende led correspondiente
    			LedOn(LED_1);
    			LedOff(LED_2);
    			LedOff(LED_3);
    			ITSE0803DisplayValue(maximo);
    			UartSendByte(config_uart.port,UartItoa(maximo, 10));
    		    	}

    		if(min_flag){		//muestra y envia el minimo y prende led correspondiente
    	    	LedOn(LED_2);
    	    	LedOff(LED_1);
    	    	LedOff(LED_3);
    	    	ITSE0803DisplayValue(minimo);
    	    	UartSendByte(config_uart.port,UartItoa(minimo, 10));
    	    		}

    		if(prom_flag){		//muestra y envia el promedio y prende led correspondiente
    			LedOn(LED_3);
    	    	LedOff(LED_2);
    	    	LedOff(LED_1);
    	    	ITSE0803DisplayValue(promedio);
    	    	UartSendByte(config_uart.port,UartItoa(promedio, 10));
    	    		 }

    		void Alarm(uint16_t maximo);  //para indicar el rango de la presion con el RGB

    		new_window=false;
    	}





	}
    
	return 0;
}

/*==================[end of file]============================================*/

