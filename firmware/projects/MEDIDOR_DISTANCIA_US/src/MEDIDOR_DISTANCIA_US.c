/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/MEDIDOR_DISTANCIA_US.h"       /* <= own header */

#include "switch.h"
#include "hc_sr4.h"
#include "systemclock.h"
#include "DisplayITS_E0803.h"
#include "gpio.h"
#include "bool.h"
#include "delay.h"
#include "led.h"

/*==================[macros and definitions]=================================*/
#define RETARDO 100
#define BLANCO 0
#define ANTI_REBOTE 100

/*==================[internal data definition]===============================*/
	bool estado_T1=false;
	bool unidad=true;//TRUE= CM ........ FALSE=INCHES
	bool hold=false;

/*==================[internal functions declaration]=========================*/
void MedicionAct(void){
	estado_T1=!estado_T1;
}

void Hold(void){
	hold=!hold;
}
void UnitCm(void){
	unidad=true;
}
void UnitInches(void){
	unidad=false;
}
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	//INICIALIZACION DE LOS DRIVERS DE DISPOSITIVOS
	SystemClockInit();
	HcSr04Init(T_FIL2,T_FIL3); // inicializamos el sensor con los pines correspondientes
	gpio_t pines[]={LCD1,LCD2,LCD3,LCD4,GPIO1,GPIO3,GPIO5};
	ITSE0803Init(pines);	 //inicializamos los pines para la pantalla
	SwitchesInit();
	LedsInit();				//inicializamos switches y leds

	//DECLARAMOS LAS VARIABLES A UTILIZAR
	uint8_t teclas;//variable que contiene la tecla apretada

	uint16_t medida=0;


	 // variables usadas en el anti rebote y estado de la tecla
	//uint8_t AR=0;



	//USAMOS LAS INTERRUPCIONES

	SwitchActivInt(SWITCH_1, MedicionAct);
	SwitchActivInt(SWITCH_2, Hold);
	SwitchActivInt(SWITCH_3, UnitCm);
	SwitchActivInt(SWITCH_4, UnitInches);



    while(1)
    {

    	if(hold==false){
    	if(estado_T1==true){    // se muestra en pantalla y se prenden el led cuando esta activada la medicion
    		LedOn(LED_RGB_B);	// teniendo en cuenta la unidad
    		LedOn(LED_RGB_R);
    		LedOn(LED_RGB_G);
    		if(unidad==true){
    		LedOff(LED_3);
    		DelayMs(RETARDO);
    		medida=HcSr04ReadDistanceCentimeters();
    		LedOn(LED_2);
    		}
    		else{
    			LedOff(LED_2);
    			DelayMs(RETARDO);
    			medida=HcSr04ReadDistanceInches();
    			LedOn(LED_3);
    		}

    		ITSE0803DisplayValue(medida);

    		}
    	else{
    		LedOff(LED_RGB_B);
    		LedOff(LED_RGB_G);
    		LedOff(LED_RGB_R);
    		ITSE0803DisplayValue(BLANCO);
    	}
    	LedOff(LED_1);
    	}
    	else{
    		LedOn(LED_1);
    		ITSE0803DisplayValue(medida);
    		}

    }


			//AR=teclas;

    
	return 0;
}

/*==================[end of file]============================================*/



//IGNORAR
//CODIGO VIEJO DE CUANDO SE HACIA SIN INTERRUPCIONES

/*teclas  = SwitchesRead();
    	//DelayMs(ANTI_REBOTE);

    if(teclas!=0&AR==0)
    	{

    	switch(teclas){

    	case SWITCH_1:
    			estado_T1=!estado_T1;   //cambiamos estado de la bandera que indica lectura y graficacion
    	break;

    	case SWITCH_2:					// corresponde al hold de la medida, prende el led y deja
    			while(hold==true){ //medida en pantalla mientras este presionada
    				LedOn(LED_1);
    				ITSE0803DisplayValue(medida);
    				teclas=SwitchesRead();
    			}
    	break;

    	case SWITCH_3:			//cambia bandera que indica la unidad con la que se mide a centimetros
    			unidad=true;
    	break;
    	case SWITCH_4:			//cambia bandera que indica la unidad con la que se mide a pulgadas
    			unidad=false;
    	break;}*/
