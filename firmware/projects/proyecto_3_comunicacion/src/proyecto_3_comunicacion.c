/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/proyecto_3_comunicacion.h"       /* <= own header */
#include "switch.h"
#include "hc_sr4.h"
#include "systemclock.h"
#include "DisplayITS_E0803.h"
#include "gpio.h"
#include "bool.h"
#include "delay.h"
#include "led.h"
#include "timer.h"
#include "uart.h"
#include"string.h"


/*==================[macros and definitions]=================================*/
#define BLANCO 0


/*==================[internal data definition]===============================*/
	bool estado_T1=false;		//ESTADO TECLA 1 ES EL QUE INDICA SI SE MIDE O NO
	bool unidad=true;			//TRUE= CM ........ FALSE=INCHES
	bool hold=false;			//BANDERA PARA EL HOLD
	bool refresco=true;
	uint16_t medida=0;
	char datos_uart[8]={0,0,0,0,0,0,0,0};  // PARA MANDAR UN BUFFER POR LA UART


/*==================[internal functions declaration]=========================*/
void MedicionAct(void){
	estado_T1=!estado_T1;
}

void Hold(void){
	hold=!hold;
}
void UnitCm(void){
	unidad=true;
}
void UnitInches(void){
	unidad=false;
}

void Refresh(void){
	refresco=true;
}

//funcion que arma el dato para ser mandado por la uart, la strcpy copia los datos convertidos por la UartItoa
// y agregamos las unidades y los saltos de linea y retorno de carro
void CargarDato(void){
	strcpy(datos_uart,UartItoa(medida,10));
	datos_uart[3]=' ';
		if(unidad==true){
			datos_uart[4]='c';
			datos_uart[5]='m';
						}
		else{
			datos_uart[4]='i';
			datos_uart[5]='n';
		}
		datos_uart[6]='\r';
		datos_uart[7]='\n';
}


void Medida(bool estado){

	if(estado_T1==true){    // se muestra en pantalla y se prenden el led cuando esta activada la medicion
	LedOn(LED_RGB_B);	// teniendo en cuenta la unidad
	LedOn(LED_RGB_R);
	LedOn(LED_RGB_G);
		if(unidad==true){
			LedOff(LED_3);
			medida=HcSr04ReadDistanceCentimeters();
			LedOn(LED_2);
			}
		else{
			LedOff(LED_2);
			medida=HcSr04ReadDistanceInches();
			LedOn(LED_3);
			}

		ITSE0803DisplayValue(medida);
		CargarDato();                                     	//Armo el dato para mandarse por la uart como un arreglo
		UartSendBuffer(SERIAL_PORT_RS485, &datos_uart, 8); 	//mando por la uart el arreglo armado anteriormente

			}
	else{
	TimerStop(TIMER_A);
	LedOff(LED_RGB_B);
	LedOff(LED_RGB_G);
	LedOff(LED_RGB_R);
	ITSE0803DisplayValue(BLANCO);
		}
	LedOff(LED_1);
}


timer_config tiempo; // VARIABLE PARA CONFIGURAR EL TIMER


/*=============configuracion de la uart===========*/

serial_config configuart={SERIAL_PORT_RS485,115200,NO_INT}; // le asignamos puerto, baudrate, el no_int es para hacer una funcion en la interrupcion cuando temrina

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	//INICIALIZACION DE LOS DRIVERS DE DISPOSITIVOS
	SystemClockInit();
	HcSr04Init(T_FIL2,T_FIL3); // inicializamos el sensor con los pines correspondientes
	gpio_t pines[]={LCD1,LCD2,LCD3,LCD4,GPIO1,GPIO3,GPIO5}; // PINES PARA LA PANTALLA
	ITSE0803Init(pines);	 //inicializamos los pines para la pantalla
	SwitchesInit();
	LedsInit();				//inicializamos switches y leds

	//DECLARAMOS LAS VARIABLES A UTILIZAR
	tiempo.timer=TIMER_A; // eleccion del timer
	tiempo.period=1000;   // Periodo del timer
	tiempo.pFunc=Medida;  // funcion que se ejecuta cada vez que la timer interrumpa


	//USAMOS LAS INTERRUPCIONES
	TimerInit(&tiempo);    // inicializamos timer

	SwitchActivInt(SWITCH_1, MedicionAct);   // se define a cada tecla activa con una interrupcion
	SwitchActivInt(SWITCH_2, Hold);
	SwitchActivInt(SWITCH_3, UnitCm);
	SwitchActivInt(SWITCH_4, UnitInches);


	//INICIALIZAMOS LA UART
	UartInit(&configuart.port);

    while(1)
    {

    	if(hold==false){
    		TimerStart(TIMER_A); //interrupcion de tiempo y esta llama a la funcion de medida
    			}
    	else{
    		TimerStop(TIMER_A);
    		LedOn(LED_1);
    		ITSE0803DisplayValue(medida);  //queda mostrando la ultima medida por pantalla
    		}

    }


    
	return 0;
}

/*==================[end of file]============================================*/

